mkdir my_workdir

cd my_workdir

git clone https://gitlab.cern.ch/vichoudi/zynq-ipbus.git/

git clone https://github.com/ipbus/ipbus-firmware.git

cd ipbus-firmware

git checkout v1.4

vivado zynq-ipbus/firmware/projects/zcu102_zynq_ipb.xpr