/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include "xparameters.h"
#include <stdio.h>
#include "platform.h"
#include <time.h>

#include "xuartps.h"	// if PS uart is used
#include "xscugic.h" 	// if PS GIC is used
#include "xil_exception.h"	// if interrupt is used

#include <stdint.h>




#define STAT       0xA0001000
#define STAT_DIR   0xA0001004
#define CTRL       0xA0001008
#define CTRL_DIR   0xA000100C
#define DIPSW      0xA0002000
#define DIPSW_DIR  0xA0002004
#define LEDS       0xA0002008
#define LEDS_DIR   0xA000200C

#define IPB        0xA0010000
#define BRAM       0xA0100000

#define C2C        0xA1000000
#define C2C_STAT   0xA1001000
#define C2C_IPB    0xA1010000
#define C2C_BRAM   0xA1100000

#define INPUT_DIR  0xFFFFFFFF
#define OUTPUT_DIR 0x00000000


int main()
{
    init_platform();

    //#################################
    //#################################
    //#################################
    //##### BASIC TESTS (LOCAL) #######
    //#################################
    //#################################
    //#################################

    for (int i= 0; i < 32; i++) print("\n\r");
    print("-----------------------------------------------------------------\n\r");
    print("#################################################################\n\r");
    print("################### LOCAL ACCESS ################################\n\r");
    print("#################################################################\n\r");
    print("-----------------------------------------------------------------\n\r");
    int32_t value; //int value;

    //Set GPIO direction
    //Xil_Out32(STAT_DIR,  INPUT_DIR);
    //Xil_Out32(CTRL_DIR,  OUTPUT_DIR);
    //Xil_Out32(DIPSW_DIR, INPUT_DIR);
    //Xil_Out32(LEDS_DIR,  OUTPUT_DIR);


    //value = Xil_In32(DIPSW);
    //printf("[info]: DIPSW    %08x\n\r", value);
    //Xil_Out32(LEDS, value);

    //BRAM test
    value = 0xcafe0000;
    for (int i= 0; i < 4; i++) {Xil_Out32(BRAM+4*i, value+i);}
    for (int i= 0; i < 4; i++) {value = Xil_In32(BRAM+4*i); printf("[info]: BRAM     %08x\n\r", value);}

    //c2c master status
    Xil_Out32(CTRL, 0);
    value = Xil_In32(CTRL);
    printf("[info]: CTRL     %08x\n\r", value);

    value = Xil_In32(STAT);
    printf("[info]: STAT     %08x\n\r", value);

    Xil_Out32(CTRL, 1);
    value = Xil_In32(CTRL);
    printf("[info]: CTRL     %08x\n\r", value);

    value = Xil_In32(STAT);
    printf("[info]: STAT     %08x\n\r", value);


    //#################################
    //#################################
    //#################################
    //##### IPBUS TESTS (LOCAL) #######
    //#################################
    //#################################
    //#################################


    print("-----------------------------------------------------------------\n\r");
    print(" example 1 [single wr]: addr -> 0x1000 data -> 0xcafe            \n\r");
    print("-----------------------------------------------------------------\n\r");
    int64_t req1[3] = {0x200000F000010003, 0x000010002000011F, 0x000000000000CAFE};
    int64_t rsp1[4];
    for (int i= 0; i < 3; i++) {Xil_Out64(IPB+8*i,req1[i]); printf("[info]: IPB REQ1 %016lx\n\r", req1[i]);} print("\n\r");
    for (int i= 0; i < 4; i++) {rsp1[i]=Xil_In64(IPB+8*i);  printf("[info]: IPB RESP %016lx\n\r", rsp1[i]);}


    print("-----------------------------------------------------------------\n\r");
    print(" example 2 [single rd] : addr -> 0x1000                          \n\r");
    print("-----------------------------------------------------------------\n\r");
    int64_t req2[2] = {0x200000F000010002, 0x000010002000010F};
    int64_t rsp2[4];
    for (int i= 0; i < 2; i++) {Xil_Out64(IPB+8*i,req2[i]); printf("[info]: IPB REQ2 %016lx\n\r", req2[i]);} print("\n\r");
    for (int i= 0; i < 4; i++) {rsp2[i]=Xil_In64(IPB+8*i);  printf("[info]: IPB RESP %016lx\n\r", rsp2[i]);}


    print("-----------------------------------------------------------------\n\r");
    print(" example 3 [combo] : wr addr -> 0x1000 data -> 0xbeef & readback \n\r");
    print("-----------------------------------------------------------------\n\r");
    int64_t req3[4] = {0x200000F000010005, 0x000010002000011F, 0x2000010F0000BEEF, 0x0000000000001000 };
    int64_t rsp3[5];
    for (int i= 0; i < 4; i++) {Xil_Out64(IPB+8*i,req3[i]); printf("[info]: IPB REQ3 %016lx\n\r", req3[i]);} print("\n\r");
    for (int i= 0; i < 5; i++) {rsp3[i]=Xil_In64(IPB+8*i);  printf("[info]: IPB RESP %016lx\n\r", rsp3[i]);}


    //#################################
    //#################################
    //#################################
    //##### BASIC TESTS (REMOTE) ######
    //#################################
    //#################################
    //#################################

    print("\n\r");
    print("-----------------------------------------------------------------\n\r");
    print("#################################################################\n\r");
    print("################### REMOTE ACCESS ###############################\n\r");
    print("#################################################################\n\r");
    print("-----------------------------------------------------------------\n\r");
    value = Xil_In32(C2C_STAT);
    printf("[info]: C2C_STAT %08x\n\r", value);

    //BRAM test
    value = 0xcafe0000;
    for (int i= 0; i < 4; i++) {Xil_Out32(C2C_BRAM+4*i, value+i);}
    for (int i= 0; i < 4; i++) {value = Xil_In32(C2C_BRAM+4*i); printf("[info]: C2C_BRAM %08x\n\r", value);}

    //#################################
    //#################################
    //#################################
    //##### IPBUS TESTS (REMOTE) ######
    //#################################
    //#################################
    //#################################


    print("-----------------------------------------------------------------\n\r");
    print(" example 1 [single wr]: addr -> 0x1000 data -> 0xcafe            \n\r");
    print("-----------------------------------------------------------------\n\r");
    int64_t req4[3] = {0x200000F000010003, 0x000010002000011F, 0x000000000000CAFE};
    int64_t rsp4[4];
    for (int i= 0; i < 3; i++) {Xil_Out64(C2C_IPB+8*i,req4[i]); printf("[info]: IPB REQ1 %016lx\n\r", req4[i]);} print("\n\r");
    for (int i= 0; i < 4; i++) {rsp4[i]=Xil_In64(C2C_IPB+8*i);  printf("[info]: IPB RESP %016lx\n\r", rsp4[i]);}


    print("-----------------------------------------------------------------\n\r");
    print(" example 2 [single rd] : addr -> 0x1000                          \n\r");
    print("-----------------------------------------------------------------\n\r");
    int64_t req5[2] = {0x200000F000010002, 0x000010002000010F};
    int64_t rsp5[4];
    for (int i= 0; i < 2; i++) {Xil_Out64(C2C_IPB+8*i,req5[i]); printf("[info]: IPB REQ5 %016lx\n\r", req5[i]);} print("\n\r");
    for (int i= 0; i < 4; i++) {rsp5[i]=Xil_In64(C2C_IPB+8*i);  printf("[info]: IPB RESP %016lx\n\r", rsp5[i]);}


    print("-----------------------------------------------------------------\n\r");
    print(" example 3 [combo] : wr addr -> 0x1000 data -> 0xbeef & readback \n\r");
    print("-----------------------------------------------------------------\n\r");
    int64_t req6[4] = {0x200000F000010005, 0x000010002000011F, 0x2000010F0000BEEF, 0x0000000000001000 };
    int64_t rsp6[5];
    for (int i= 0; i < 4; i++) {Xil_Out64(C2C_IPB+8*i,req6[i]); printf("[info]: IPB REQ3 %016lx\n\r", req6[i]);} print("\n\r");
    for (int i= 0; i < 5; i++) {rsp6[i]=Xil_In64(C2C_IPB+8*i);  printf("[info]: IPB RESP %016lx\n\r", rsp6[i]);}

    cleanup_platform();
    return 0;
}
