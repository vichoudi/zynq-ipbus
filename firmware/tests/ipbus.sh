STAT=0xA0001000
STAT_DIR=0xA0001004
CTRL=0xA0001008
CTRL_DIR=0xA000100C
DIPSW=0xA0002000
DIPSW_DIR=0xA0002004
LEDS=0xA0002008
LEDS_DIR=0xA000200C

IPB=0xA0010000
BRAM=0xA0100000

C2C=0xA1000000
C2C_STAT=0xA1001000
C2C_IPB=0xA1010000
C2C_BRAM=0xA1100000

INPUT_DIR=0xFFFFFFFF
OUTPUT_DIR=0x00000000


printf "\n\n\n\n\n"
printf "=================================================================\n"
printf "#################################################################\n"
printf "################### LOCAL ACCESS ################################\n"
printf "#################################################################\n"
printf "=================================================================\n"

#c2c master status
#poke $CTRL 0
#value=$(peek $CTRL) 
#printf "[info]: CTRL     %08x\n" $value

value=$(peek $STAT) 
printf "[info]: STAT     %08x\n" $value    

poke $CTRL 1
value=$(peek $CTRL) 
printf "[info]: CTRL     %08x\n" $value

value=$(peek $STAT) 
printf "[info]: STAT     %08x\n" $value 

#BRAM test
for i in {0..3}
do
  value=$((0xcafe0000+$i))
  addr=$(($BRAM+4*$i))
  poke $addr $value
#  printf "[info]: wr %08x %08x\n" $addr $value
done

for i in {0..3}
do
  addr=$(($BRAM+4*$i))
  value=$(peek $addr)
  printf "[info]: BRAM     %08x %08x\n" $addr $value
done  


 
 
#    //#################################
#    //#################################
#    //#################################
#    //##### IPBUS TESTS (LOCAL) #######
#    //#################################
#    //#################################
#    //#################################
  
printf "=================================================================\n"
printf " example 1 [single wr]: addr -> 0x1000 data -> 0xcafe            \n"
printf "=================================================================\n"

req1=(0x200000F000010003 0x000010002000011F 0x000000000000CAFE)

for i in {0..2}
do
  addr=$(($IPB+8*$i))
  value=${req1[$i]}
  poke64 $addr $value
  printf "[info]: request  %08x %016lx\n" $addr $value
done
printf "\n"
for i in {0..3}
do
  addr=$(($IPB+8*$i))
  value=$(peek64 $addr)
  poke64 $addr $value
  printf "[info]: response %08x %016lx\n" $addr $value
done    
 
printf "=================================================================\n"
printf " example 2 [single rd] : addr -> 0x1000                          \n"
printf "=================================================================\n"
req2=(0x200000F000010002 0x000010002000010F)

for i in {0..1}
do
  addr=$(($IPB+8*$i))
  value=${req2[$i]}
  poke64 $addr $value
  printf "[info]: request  %08x %016lx\n" $addr $value
done
printf "\n"
for i in {0..3}
do
  addr=$(($IPB+8*$i))
  value=$(peek64 $addr)
  poke64 $addr $value
  printf "[info]: response %08x %016lx\n" $addr $value
done 

 
 
printf "=================================================================\n"
printf " example 3 [combo] : wr addr -> 0x1000 data -> 0xbeef & readback \n"
printf "=================================================================\n"
req3=(0x200000F000010005 0x000010002000011F 0x2000010F0000BEEF 0x0000000000001000)
for i in {0..3}
do
  addr=$(($IPB+8*$i))
  value=${req3[$i]}
  poke64 $addr $value
  printf "[info]: request  %08x %016lx\n" $addr $value
done
printf "\n"
for i in {0..4}
do
  addr=$(($IPB+8*$i))
  value=$(peek64 $addr)
  poke64 $addr $value
  printf "[info]: response %08x %016lx\n" $addr $value
done 
      
 

 
printf "\n\n\n"
printf "=================================================================\n"
printf "#################################################################\n"
printf "################### REMOTE ACCESS ###############################\n"
printf "#################################################################\n"
printf "=================================================================\n"


#c2c slave status
value=$(peek $C2C_STAT) 
printf "[info]: C2C STAT %08x\n" $value  
 
#BRAM test
for i in {0..3}
do

  value=$((0xdead0000+$i))
  addr=$(($C2C_BRAM+4*$i))
  poke $addr $value
#  printf "[info]: wr %08x %08x\n" $addr $value
done

for i in {0..3}
do
  addr=$(($C2C_BRAM+4*$i))
  value=$(peek $addr)
  printf "[info]: C2C BRAM %08x %08x\n" $addr $value
done
 
#    //#################################
#    //#################################
#    //#################################
#    //##### IPBUS TESTS (REMOTE) ######
#    //#################################
#    //#################################
#    //#################################

printf "=================================================================\n"
printf " example 1 [single wr]: addr -> 0x1000 data -> 0xcafe            \n"
printf "=================================================================\n"

req4=(0x200000F000010003 0x000010002000011F 0x000000000000CAFE)

for i in {0..2}
do
  addr=$(($C2C_IPB+8*$i))
  value=${req4[$i]}
  poke64 $addr $value
  printf "[info]: request  %08x %016lx\n" $addr $value
done
printf "\n"
for i in {0..3}
do
  addr=$(($C2C_IPB+8*$i))
  value=$(peek64 $addr)
  poke64 $addr $value
  printf "[info]: response %08x %016lx\n" $addr $value
done    
 
printf "=================================================================\n"
printf " example 2 [single rd] : addr -> 0x1000                          \n"
printf "=================================================================\n"
req5=(0x200000F000010002 0x000010002000010F)

for i in {0..1}
do
  addr=$(($C2C_IPB+8*$i))
  value=${req5[$i]}
  poke64 $addr $value
  printf "[info]: request  %08x %016lx\n" $addr $value
done
printf "\n"
for i in {0..3}
do
  addr=$(($C2C_IPB+8*$i))
  value=$(peek64 $addr)
  poke64 $addr $value
  printf "[info]: response %08x %016lx\n" $addr $value
done 

 
 
printf "=================================================================\n"
printf " example 3 [combo] : wr addr -> 0x1000 data -> 0xbeef & readback \n"
printf "=================================================================\n"
req6=(0x200000F000010005 0x000010002000011F 0x2000010F0000BEEF 0x0000000000001000)
for i in {0..3}
do
  addr=$(($C2C_IPB+8*$i))
  value=${req6[$i]}
  poke64 $addr $value
  printf "[info]: request  %08x %016lx\n" $addr $value
done
printf "\n"
for i in {0..4}
do
  addr=$(($C2C_IPB+8*$i))
  value=$(peek64 $addr)
  poke64 $addr $value
  printf "[info]: response %08x %016lx\n" $addr $value
done  
 


printf "\n\n\n"